const makeUpSentence = [
    {
        title: "Present Simple",
        id: 0,
        info: [
            {
                test_title: 'тест 1',
                image: null,
                questions: [
                    {id: 1, sample: 'The children ... berries in the forest.', options: [{answer: 'pick', isCorrect: true}, {answer: 'picks', isCorrect: false}]},
                    {id: 2, sample: 'Mum often ... mushrooms.', options: [{answer: 'cook', isCorrect: false}, {answer: 'cooks', isCorrect: true}]},
                    {id: 3, sample: 'The child ... to swing in the garden.', options: [{answer: 'like', isCorrect: false}, {answer: 'likes', isCorrect: true}]},
                    {id: 4, sample: 'My cat ... mice very well.', options: [{answer: 'catch', isCorrect: false}, {answer: 'catches', isCorrect: true}]},
                    {id: 5, sample: 'Jack in the park every day.', options: [{answer: 'roller skate', isCorrect: false}, {answer: 'roller skates', isCorrect: true}]},
                    {id: 6, sample: 'Polly and her sister ... flowers every summer.', options: [{answer: 'grow', isCorrect: true}, {answer: 'grows', isCorrect: false}]},
                    {id: 7, sample: 'Her parents ... a lot during their holidays.', options: [{answer: 'travel', isCorrect: true}, {answer: 'travels', isCorrect: false}]},
                    {id: 8, sample: 'I ... my dog out every morning.', options: [{answer: 'walk', isCorrect: true}, {answer: 'walks', isCorrect: false}]},
                    {id: 9, sample: 'The birds ... nicely in my garden.', options: [{answer: 'sing', isCorrect: true}, {answer: 'sings', isCorrect: false}]},
                    {id: 10, sample: 'Our teacher ... us a lot of homework.', options: [{answer: 'give', isCorrect: false}, {answer: 'gives', isCorrect: true}]}
                ]
            }
        ]
    },
    {
        title: "Past Simple",
        id: 1,
        info: [
            {
                test_title: 'тест 1',
                image: null,
                questions: [
                    {id: 1, sample: 'There ... not many apples in the basket.', options: [{answer: 'is', isCorrect: false}, {answer: 'are', isCorrect: true}]},
                    {id: 2, sample: 'There ... 22 pupils in my class.', options: [{answer: 'are', isCorrect: true}, {answer: 'is', isCorrect: false}]},
                    {id: 3, sample: '... there any mice in the pantry?', options: [{answer: 'Are', isCorrect: true}, {answer: 'Is', isCorrect: false}]},
                    {id: 4, sample: 'There ... a nice bird in the tree.', options: [{answer: 'is', isCorrect: true}, {answer: 'are', isCorrect: false}]},
                    {id: 5, sample: 'There ... some big houses in the picture.', options: [{answer: 'is', isCorrect: false}, {answer: 'are', isCorrect: true}]},
                    {id: 6, sample: 'There ... not a cloud in the sky.', options: [{answer: 'is', isCorrect: true}, {answer: 'are', isCorrect: false}]},
                    {id: 7, sample: '... there foxes in the forest?', options: [{answer: 'Are', isCorrect: true}, {answer: 'Is', isCorrect: false}]},
                    {id: 8, sample: 'There ... twelve months in the year.', options: [{answer: 'are', isCorrect: true}, {answer: 'is', isCorrect: false}]},
                    {id: 9, sample: '... there any pencils in your bag?', options: [{answer: 'is', isCorrect: false}, {answer: 'are', isCorrect: true}]},
                    {id: 10, sample: 'There ... a pet in my family.', options: [{answer: 'is', isCorrect: true}, {answer: 'are', isCorrect: false}]}
                ]
            }
        ]
    }
]

export default makeUpSentence;