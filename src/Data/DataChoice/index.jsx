const dataChoice = [
    {
        title: "Множественное число имен существительных",
        id: 0,
        info: [
            {
                test_title: 'тест 1',
                image: null,
                questions: [
                    {id: 1, sample: 'Many ... live in the forest.', options: [{answer: 'foxs', isCorrect: false}, {answer: 'foxes', isCorrect: true}]},
                    {id: 2, sample: 'Four ... are on the table.', options: [{answer: 'muges', isCorrect: false}, {answer: 'mugs', isCorrect: true}]},
                    {id: 3, sample: 'I see ... in the street.', options: [{answer: 'cars', isCorrect: true}, {answer: 'cares', isCorrect: false}]},
                    {id: 4, sample: 'We have nice ... in the park.', options: [{answer: 'benchs', isCorrect: false}, {answer: 'benches', isCorrect: true}]},
                    {id: 5, sample: "I have two ... .", options: [{answer: 'mapes', isCorrect: false}, {answer: 'maps', isCorrect: true}]},
                    {id: 6, sample: 'Many ... live in big houses.', options: [{answer: 'familys', isCorrect: false}, {answer: 'families', isCorrect: true}]},
                    {id: 7, sample: 'I see ... in the garden.', options: [{answer: 'cockes', isCorrect: false}, {answer: 'cocks', isCorrect: true}]},
                    {id: 8, sample: 'I have five ... in the room.', options: [{answer: 'shelves', isCorrect: true}, {answer: 'shelfs', isCorrect: false}]},
                    {id: 9, sample: 'We see red ... near the house.', options: [{answer: 'flowers', isCorrect: true}, {answer: 'floweres', isCorrect: false}]},
                    {id: 10, sample: 'We see many ... in the sky.', options: [{answer: 'stares', isCorrect: false}, {answer: 'stars', isCorrect: true}]}
                ]
            },
            {
                test_title: 'тест 2',
                image: null,
                questions: [
                    {id: 1, sample: 'John has got seven ... .', options: [{answer: 'chickenes', isCorrect: false}, {answer: 'chickens', isCorrect: true}, {answer: 'chickenns', isCorrect: false}]},
                    {id: 2, sample: 'The ... play football.', options: [{answer: 'childs', isCorrect: false}, {answer: 'children', isCorrect: true}, {answer: 'childrens', isCorrect: false}]},
                    {id: 3, sample: "Bob's ... are very cold.", options: [{answer: 'foots', isCorrect: false}, {answer: 'feet', isCorrect: true}, {answer: 'footts', isCorrect: false}]},
                    {id: 4, sample: 'The ... are my aunts.', options: [{answer: 'womanns', isCorrect: false}, {answer: 'womans', isCorrect: false}, {answer: 'women', isCorrect: true}]},
                    {id: 5, sample: 'The ... red, orange and yellow.', options: [{answer: 'fish', isCorrect: false}, {answer: 'fishes', isCorrect: true}, {answer: 'fishs', isCorrect: false}]},
                    {id: 6, sample: 'The boy has got twenty ... .', options: [{answer: 'teeth', isCorrect: true}, {answer: 'tooth', isCorrect: false}, {answer: 'tooths', isCorrect: false}]},
                    {id: 7, sample: 'The ... are in the sandbox.', options: [{answer: 'babys', isCorrect: false}, {answer: 'babyes', isCorrect: false}, {answer: 'babies', isCorrect: true}]},
                    {id: 8, sample: 'The ... are in the house.', options: [{answer: 'mouses', isCorrect: false}, {answer: 'mice', isCorrect: true}, {answer: 'mices', isCorrect: false}]},
                    {id: 9, sample: 'The ... live in the North.', options: [{answer: 'reindeer', isCorrect: false}, {answer: 'reindeers', isCorrect: true}, {answer: 'reindeeres', isCorrect: false}]},
                    {id: 10, sample: 'We see big ... in the photo.', options: [{answer: 'oxen', isCorrect: true}, {answer: 'oxes', isCorrect: false}, {answer: 'oxs', isCorrect: false}]}
                ]
            },
            {
                test_title: 'тест 3',
                image: null,
                questions: [
                    {id: 1, sample: 'Mike likes reading books about ... .', options: [{answer: 'fairys', isCorrect: false}, {answer: 'fairyes', isCorrect: false}, {answer: 'fairies', isCorrect: true}]},
                    {id: 2, sample: 'The peakock has got beautiful ... .', options: [{answer: 'feathers', isCorrect: true}, {answer: 'featheres', isCorrect: false}, {answer: 'feather', isCorrect: false}]},
                    {id: 3, sample: 'Mum loves ... .', options: [{answer: 'lilys', isCorrect: false}, {answer: 'lilies', isCorrect: true}, {answer: 'lilyes', isCorrect: false}]},
                    {id: 4, sample: 'First ... wake the girl up.', options: [{answer: 'rayes', isCorrect: false}, {answer: 'raies', isCorrect: false}, {answer: 'rays', isCorrect: true}]},
                    {id: 5, sample: 'There are nice ... in our park.', options: [{answer: 'bushs', isCorrect: false}, {answer: 'bushes', isCorrect: true}, {answer: 'bush', isCorrect: false}]},
                    {id: 6, sample: 'There are two ... in the town.', options: [{answer: 'pitches', isCorrect: true}, {answer: 'pitch', isCorrect: false}, {answer: 'pitchs', isCorrect: false}]},
                    {id: 7, sample: 'I like ... .', options: [{answer: 'tomatos', isCorrect: false}, {answer: 'tomatoes', isCorrect: true}, {answer: 'tomato', isCorrect: false}]},
                    {id: 8, sample: 'They sell ... in the shop.', options: [{answer: 'radioes', isCorrect: false}, {answer: 'radios', isCorrect: true}, {answer: 'radio', isCorrect: false}]},
                    {id: 9, sample: 'The story is about these ... .', options: [{answer: 'elfs', isCorrect: false}, {answer: 'elfes', isCorrect: false}, {answer: 'elves', isCorrect: true}]},
                    {id: 10, sample: 'There are three ... in my room.', options: [{answer: 'shelves', isCorrect: true}, {answer: 'shelf', isCorrect: false}, {answer: 'shelfes', isCorrect: false}]}
                ]
            },
            {
                test_title: 'тест 4',
                image: null,
                questions: [
                    {id: 1, sample: "Jack didn't find his ... in the backpack.", options: [{answer: 'gloves', isCorrect: false}, {answer: 'glofs', isCorrect: false}, {answer: 'glovs', isCorrect: true}]},
                    {id: 2, sample: 'These ... danced beautifully at the concert.', options: [{answer: 'womans', isCorrect: true}, {answer: 'women', isCorrect: false}, {answer: 'womanes', isCorrect: false}]},
                    {id: 3, sample: 'We can see some ... in the field.', options: [{answer: 'gooses', isCorrect: false}, {answer: 'geeses', isCorrect: true}, {answer: 'geese', isCorrect: false}]},
                    {id: 4, sample: 'My cat caught two ... .', options: [{answer: 'mouses', isCorrect: false}, {answer: 'mice', isCorrect: true}, {answer: 'mouse', isCorrect: false}]},
                    {id: 5, sample: 'There are a lot of ... at the party today.', options: [{answer: 'persons', isCorrect: false}, {answer: 'people', isCorrect: true}, {answer: 'peoples', isCorrect: false}]},
                    {id: 6, sample: 'These ... are from our school.', options: [{answer: 'childs', isCorrect: true}, {answer: 'childrens', isCorrect: false}, {answer: 'children', isCorrect: false}]},
                    {id: 7, sample: 'He brushes his ... every morning.', options: [{answer: 'tooth', isCorrect: false}, {answer: 'toothes', isCorrect: true}, {answer: 'teeth', isCorrect: false}]},
                    {id: 8, sample: 'Where are my ... ?', options: [{answer: 'keys', isCorrect: false}, {answer: 'keies', isCorrect: true}, {answer: 'keyes', isCorrect: false}]},
                    {id: 9, sample: 'How many ... are there in the aquarium?', options: [{answer: 'fishes', isCorrect: false}, {answer: 'fish', isCorrect: false}, {answer: 'fishs', isCorrect: true}]}
                ]
            },
            {
                test_title: 'тест 5',
                image: null,
                questions: [
                    {id: 1, sample: "She has got two nice ... .", options: [{answer: 'box', isCorrect: false},{answer: 'boxs', isCorrect: false},{answer: 'boxes', isCorrect: true}]},
                    {id: 2, sample: 'I see a lot of ... in the bag.', options: [{answer: 'potatos', isCorrect: true},{answer: 'potatoes', isCorrect: false},{answer: 'potato', isCorrect: false}]},
                    {id: 3, sample: 'We see five ... in the field.', options: [{answer: 'sheep', isCorrect: false},{answer: 'sheeps', isCorrect: true},{answer: 'sheepes', isCorrect: false}]},
                    {id: 4, sample: 'Take these ... .', options: [{answer: 'pencilles', isCorrect: false},{answer: 'pencils', isCorrect: true},{answer: 'penciles', isCorrect: false}]},
                    {id: 5, sample: 'There is three ... .', options: [{answer: 'busses', isCorrect: false},{answer: 'buses', isCorrect: true},{answer: 'buss', isCorrect: false}]},
                    {id: 6, sample: 'The ... are in the shop.', options: [{answer: 'ladys', isCorrect: true},{answer: 'ladies', isCorrect: false},{answer: 'lady', isCorrect: false}]},
                    {id: 7, sample: 'I play football with the ... .', options: [{answer: 'boys', isCorrect: false},{answer: 'boyes', isCorrect: true},{answer: 'boies', isCorrect: false}]},
                    {id: 8, sample: 'There are nice ... in the shop.', options: [{answer: 'watchs', isCorrect: false}, {answer: 'watches', isCorrect: true}, {answer: 'watch', isCorrect: false}]},
                    {id: 9, sample: 'We have two ... in the zoo.', options: [{answer: 'elephants', isCorrect: false},{answer: 'elephantes', isCorrect: false},{answer: 'elephanttes', isCorrect: true}]},
                    {id: 10, sample: '... live in the forest.', options: [{answer: 'Wolves', isCorrect: true},{answer: 'Wolfs', isCorrect: false},{answer: 'Wolfes', isCorrect: false}]}
                ]
            },
            {
                test_title: 'тест 6',
                image: null,
                questions: [
                    {id: 1, sample: 'Show me these ... , please.', options: [{answer: 'photoes', isCorrect: false},{answer: 'photos', isCorrect: false},{answer: 'photo', isCorrect: true}]},
                    {id: 2, sample: 'The children listen to the ... before going to bed.', options: [{answer: 'stories', isCorrect: true},{answer: 'storyes', isCorrect: false},{answer: 'storys', isCorrect: false}]},
                    {id: 3, sample: "Let's water new ... on Youtube.", options: [{answer: 'videos', isCorrect: false},{answer: 'videoes', isCorrect: true},{answer: 'video', isCorrect: false}]},
                    {id: 4, sample: 'There are some ... in the basket.', options: [{answer: 'peachs', isCorrect: false},{answer: 'peaches', isCorrect: true},{answer: 'peach', isCorrect: false}]},
                    {id: 5, sample: 'The ... are playing in the sandbox.', options: [{answer: 'babies', isCorrect: false},{answer: 'babys', isCorrect: true},{answer: 'babyes', isCorrect: false}]},
                    {id: 6, sample: 'There are a lot of ... in the city.', options: [{answer: 'buss', isCorrect: false},{answer: 'buses', isCorrect: true},{answer: 'bus', isCorrect: false}]},
                    {id: 7, sample: 'These ... are our new drivers.', options: [{answer: 'mans', isCorrect: false},{answer: 'men', isCorrect: true},{answer: 'mens', isCorrect: false}]},
                    {id: 8, sample: 'My ... are cold.', options: [{answer: 'foots', isCorrect: false}, {answer: 'feets', isCorrect: true}, {answer: 'feet', isCorrect: false}]},
                    {id: 9, sample: 'I can see big ... in the picture.', options: [{answer: 'oxes', isCorrect: false},{answer: 'oxen', isCorrect: false},{answer: 'oxs', isCorrect: true}]},
                    {id: 10, sample: 'There are some ... on  the farm.', options: [{answer: 'sheeps', isCorrect: true},{answer: 'sheep', isCorrect: false},{answer: 'sheepes', isCorrect: false}]}
                ]
            }
        ]
    },
    {
        title: "Местоимения",
        id: 1,
        info: [
            {
                test_title: 'тест 1',
                image: null,
                questions: [
                    {id: 1, sample: 'John is 32 years old. ... is a teacher.', options: [{answer: 'She', isCorrect: false},{answer: 'He', isCorrect: true},{answer: 'I', isCorrect: false}]},
                    {id: 2, sample: 'My friend and I are at school now. ... are having English now.', options: [{answer: 'They', isCorrect: false},{answer: 'You', isCorrect: false},{answer: 'We', isCorrect: true}]},
                    {id: 3, sample: 'The children are outdoors. ... are in the sandbox.', options: [{answer: 'We', isCorrect: false},{answer: 'They', isCorrect: true},{answer: 'You', isCorrect: false}]},
                    {id: 4, sample: 'Is your car new? - Yes, ... is.', options: [{answer: 'she', isCorrect: false},{answer: 'it', isCorrect: true},{answer: 'I', isCorrect: false}]},
                    {id: 5, sample: 'You and Jill are cousins. Are ... both ten years old?', options: [{answer: 'we', isCorrect: false},{answer: 'she', isCorrect: false},{answer: 'you', isCorrect: true}]},
                    {id: 6, sample: 'Lions are wild animals. ... are very dangerous.', options: [{answer: 'It', isCorrect: false},{answer: 'They', isCorrect: true},{answer: 'Their', isCorrect: false}]},
                    {id: 7, sample: "John's dog is funny. ... is very kind.", options: [{answer: 'He', isCorrect: false},{answer: 'I', isCorrect: false},{answer: 'It', isCorrect: true}]},
                    {id: 8, sample: 'Are your cats hungry? - Yes, ... are.', options: [{answer: 'we', isCorrect: false}, {answer: 'I', isCorrect: false}, {answer: 'they', isCorrect: true}]},
                    {id: 9, sample: 'Is your elder brother at work? - Yes, ... is.', options: [{answer: 'she', isCorrect: false},{answer: 'I', isCorrect: false},{answer: 'he', isCorrect: true}]},
                    {id: 10, sample: 'Are you and Tom friends? - Yes, ... are.', options: [{answer: 'I', isCorrect: false},{answer: 'we', isCorrect: true},{answer: 'they', isCorrect: false}]}
                ]
            },
            {
                test_title: 'тест 2',
                image: null,
                questions: [
                    {id: 1, sample: 'Dolly enjoys dancing and ... sister likes singing.', options: [{answer: 'my', isCorrect: false},{answer: 'her', isCorrect: true},{answer: 'their', isCorrect: false}]},
                    {id: 2, sample: 'Where are you now? ... mum is looking for you.', options: [{answer: 'My', isCorrect: false},{answer: 'Our', isCorrect: false},{answer: 'Your', isCorrect: true}]},
                    {id: 3, sample: 'Do you like the fairy tale? ... name is "Cinderella".', options: [{answer: 'Her', isCorrect: true},{answer: 'Its', isCorrect: false},{answer: 'Their', isCorrect: false}]},
                    {id: 4, sample: "My friend and I aren't outdoors today. ... bikes are broken.", options: [{answer: 'Our', isCorrect: true},{answer: 'Their', isCorrect: false},{answer: 'My', isCorrect: false}]},
                    {id: 5, sample: 'Where is your sister? ... friends are waiting for her.', options: [{answer: 'Her', isCorrect: true},{answer: 'His', isCorrect: false},{answer: 'Our', isCorrect: false}]},
                    {id: 6, sample: 'Look at the giraffes. ... necks are so long.', options: [{answer: 'Their', isCorrect: true},{answer: 'Its', isCorrect: false},{answer: 'Our', isCorrect: false}]},
                    {id: 7, sample: "I don't go to work on Monday. It is ... day-off.", options: [{answer: 'our', isCorrect: false},{answer: 'his', isCorrect: false},{answer: 'my', isCorrect: true}]},
                    {id: 8, sample: "We don't go to school on Saturday. It is ... day-off.", options: [{answer: 'my', isCorrect: false}, {answer: 'their', isCorrect: false}, {answer: 'our', isCorrect: true}]},
                    {id: 9, sample: 'Alexander and Mihail are brothers. ... parents are nice people.', options: [{answer: 'Our', isCorrect: false},{answer: 'My', isCorrect: false},{answer: 'Their', isCorrect: true}]},
                    {id: 10, sample: 'Mr. Smith is a doctor. ... patients love him.', options: [{answer: 'Her', isCorrect: false},{answer: 'Our', isCorrect: false},{answer: 'His', isCorrect: true}]}
                ]
            },
            {
                test_title: 'тест 3',
                image: null,
                questions: [
                    {id: 1, sample: 'Dorathy has got a new bike. ... bike is red.', options: [{answer: 'She', isCorrect: false},{answer: 'Her', isCorrect: true},{answer: 'Its', isCorrect: false}]},
                    {id: 2, sample: '... school is big and modern.', options: [{answer: 'We', isCorrect: false},{answer: 'Our', isCorrect: true},{answer: 'Us', isCorrect: false}]},
                    {id: 3, sample: '... teacher is very helpful.', options: [{answer: 'I', isCorrect: false},{answer: 'Me', isCorrect: false},{answer: 'My', isCorrect: true}]},
                    {id: 4, sample: "Do you see a monkey? ... tail is long.", options: [{answer: 'Her', isCorrect: false},{answer: 'It', isCorrect: false},{answer: 'Its', isCorrect: true}]},
                    {id: 5, sample: '... dad is a driver.', options: [{answer: 'He', isCorrect: false},{answer: 'Him', isCorrect: false},{answer: 'His', isCorrect: true}]},
                    {id: 6, sample: '... hair is long.', options: [{answer: 'She', isCorrect: false},{answer: 'Him', isCorrect: false},{answer: 'Her', isCorrect: true}]},
                    {id: 7, sample: "... car is in the garage.", options: [{answer: 'We', isCorrect: false},{answer: 'Our', isCorrect: true},{answer: 'Them', isCorrect: false}]},
                    {id: 8, sample: "Granny has got a garden. There are a lot of tree apples in ... garden.", options: [{answer: 'our', isCorrect: false}, {answer: 'her', isCorrect: true}, {answer: 'his', isCorrect: false}]},
                    {id: 9, sample: 'Jack is cold. ... feet are wet.', options: [{answer: 'Him', isCorrect: false},{answer: 'Her', isCorrect: false},{answer: 'His', isCorrect: true}]},
                    {id: 10, sample: 'I have got a teddy bear. It is ... favourite toy.', options: [{answer: 'me', isCorrect: false},{answer: 'my', isCorrect: true},{answer: 'their', isCorrect: false}]}
                ]
            },
            {
                test_title: 'тест 4',
                image: null,
                questions: [
                    {id: 1, sample: '... legs are short.', options: [{answer: 'It', isCorrect: false},{answer: 'Its', isCorrect: true},{answer: 'We', isCorrect: false}]},
                    {id: 2, sample: 'Is ... dangerous?', options: [{answer: 'its', isCorrect: false},{answer: 'we', isCorrect: false},{answer: 'it', isCorrect: true}]},
                    {id: 3, sample: '... are in the park now.', options: [{answer: 'Our', isCorrect: false},{answer: 'My', isCorrect: false},{answer: 'We', isCorrect: true}]},
                    {id: 4, sample: '... computer is old.', options: [{answer: 'I', isCorrect: false},{answer: 'Me', isCorrect: false},{answer: 'My', isCorrect: true}]},
                    {id: 5, sample: '... is a good pupil.', options: [{answer: 'I', isCorrect: false},{answer: 'His', isCorrect: false},{answer: 'He', isCorrect: true}]},
                    {id: 6, sample: '... is a beautiful girl.', options: [{answer: 'He', isCorrect: false},{answer: 'She', isCorrect: true},{answer: 'Her', isCorrect: false}]},
                    {id: 7, sample: '... toys are in the box.', options: [{answer: 'His', isCorrect: true},{answer: 'Him', isCorrect: false},{answer: 'He', isCorrect: false}]},
                    {id: 8, sample: 'Is ... dad at home?', options: [{answer: 'he', isCorrect: false}, {answer: 'your', isCorrect: true}, {answer: 'you', isCorrect: false}]},
                    {id: 9, sample: '... books are interesting.', options: [{answer: 'They', isCorrect: false},{answer: 'Them', isCorrect: false},{answer: 'Their', isCorrect: true}]},
                    {id: 10, sample: '... am a student.', options: [{answer: 'Me', isCorrect: false},{answer: 'I', isCorrect: true},{answer: 'My', isCorrect: false}]}
                ]
            },
            {
                test_title: 'тест 5',
                image: null,
                questions: [
                    {id: 1, sample: 'How many sweets did ... eat?', options: [{answer: 'his', isCorrect: false},{answer: 'he', isCorrect: true}]},
                    {id: 2, sample: 'Do ... help you?', options: [{answer: 'their', isCorrect: false},{answer: 'they', isCorrect: true}]},
                    {id: 3, sample: "I can't find ... glasses.", options: [{answer: 'I', isCorrect: false},{answer: 'my', isCorrect: true}]},
                    {id: 4, sample: 'Is ... colour green or grey?', options: [{answer: 'they', isCorrect: false},{answer: 'their', isCorrect: true}]},
                    {id: 5, sample: 'Where did ... buy the trainers?', options: [{answer: 'your', isCorrect: false},{answer: 'you', isCorrect: true}]},
                    {id: 6, sample: '... dress is pink.', options: [{answer: 'I', isCorrect: false},{answer: 'My', isCorrect: true}]},
                    {id: 7, sample: '... is so interesting!', options: [{answer: 'Its', isCorrect: false},{answer: 'It', isCorrect: true}]},
                    {id: 8, sample: 'How many rooms are there in ... flat?', options: [{answer: 'you', isCorrect: false}, {answer: 'your', isCorrect: true}]},
                    {id: 9, sample: 'Where do ... live?', options: [{answer: 'your', isCorrect: false},{answer: 'you', isCorrect: true}]},
                    {id: 10, sample: "I don't remember ... address.", options: [{answer: 'they', isCorrect: false},{answer: 'their', isCorrect: true}]}
                ]
            },
            {
                test_title: 'тест 6',
                image: null,
                questions: [
                    {id: 1, sample: '... relatives live in France.', options: [{answer: 'our', isCorrect: true},{answer: 'ours', isCorrect: false}]},
                    {id: 2, sample: 'My name is Bob. What is ... ?', options: [{answer: 'yours', isCorrect: true},{answer: 'your', isCorrect: false}]},
                    {id: 3, sample: 'His name is George. What is ... ?', options: [{answer: 'her', isCorrect: false},{answer: 'hers', isCorrect: true}]},
                    {id: 4, sample: 'Are ... parents from the USA?', options: [{answer: 'their', isCorrect: true},{answer: 'theirs', isCorrect: false}]},
                    {id: 5, sample: 'Our dog is old. ... is younger?', options: [{answer: 'Your', isCorrect: false},{answer: 'Yours', isCorrect: true}]},
                    {id: 6, sample: 'I left my phone at home. Can you give me ... ?', options: [{answer: 'your', isCorrect: false},{answer: 'yours', isCorrect: true}]},
                    {id: 7, sample: "This umbrella isn't ... .", options: [{answer: 'my', isCorrect: false},{answer: 'mine', isCorrect: true}]},
                    {id: 8, sample: '... grandparents live in the country.', options: [{answer: 'My', isCorrect: true}, {answer: 'Mine', isCorrect: false}]},
                    {id: 9, sample: 'Is it ... bag?', options: [{answer: 'your', isCorrect: true},{answer: 'yours', isCorrect: false}]},
                    {id: 10, sample: 'How many bedrooms are there in ... house?', options: [{answer: 'your', isCorrect: true},{answer: 'yours', isCorrect: false}]}
                ]
            },
            {
                test_title: 'тест 7',
                image: null,
                questions: [
                    {id: 1, sample: 'Mike takes ... dog for a walk twice a day.', options: [{answer: 'him', isCorrect: false},{answer: 'he', isCorrect: false},{answer: 'his', isCorrect: true}]},
                    {id: 2, sample: 'Ben and Max are ... cousins.', options: [{answer: 'my', isCorrect: true},{answer: 'mine', isCorrect: false},{answer: 'yours', isCorrect: false}]},
                    {id: 3, sample: 'The teachers gives ... interesting tasks.', options: [{answer: 'their', isCorrect: false},{answer: 'his', isCorrect: false},{answer: 'them', isCorrect: true}]},
                    {id: 4, sample: 'Where are ... now?', options: [{answer: 'her', isCorrect: false},{answer: 'she', isCorrect: false},{answer: 'they', isCorrect: true}]},
                    {id: 5, sample: 'Did ... close ... door?', options: [{answer: 'you, your', isCorrect: true},{answer: 'your, you', isCorrect: false},{answer: 'his, him', isCorrect: false}]},
                    {id: 6, sample: "I can't call ... now.", options: [{answer: 'your', isCorrect: false},{answer: 'their', isCorrect: false},{answer: 'her', isCorrect: true}]},
                    {id: 7, sample: 'Give ... some more time.', options: [{answer: 'my', isCorrect: false},{answer: 'us', isCorrect: true},{answer: 'they', isCorrect: false}]},
                    {id: 8, sample: 'Are ... friends in the park now?', options: [{answer: 'they', isCorrect: false}, {answer: 'them', isCorrect: false}, {answer: 'their', isCorrect: true}]},
                    {id: 9, sample: '... sister plays badminton with ... .', options: [{answer: 'His, their', isCorrect: false},{answer: 'My, me', isCorrect: true},{answer: 'Their, his', isCorrect: false}]},
                    {id: 10, sample: "I don't know ... name.", options: [{answer: 'it', isCorrect: false},{answer: 'its', isCorrect: true},{answer: 'him', isCorrect: false}]}
                ]
            }
        ]
    },
    {
        title: "Указательные местоимения",
        id: 2,
        info: [
            {
                test_title: 'тест 1',
                image: null,
                questions: [
                    {id: 1, sample: '... people are doctors.', options: [{answer: 'This', isCorrect: false}, {answer: 'These', isCorrect: true}, {answer: 'That', isCorrect: false}]},
                    {id: 2, sample: '... fish is colourful.', options: [{answer: 'Those', isCorrect: false}, {answer: 'This', isCorrect: true}, {answer: 'These', isCorrect: false}]},
                    {id: 3, sample: '... city is old.', options: [{answer: 'Those', isCorrect: false}, {answer: 'These', isCorrect: false}, {answer: 'This', isCorrect: true}]},
                    {id: 4, sample: '... toys are expensive.', options: [{answer: 'This', isCorrect: false}, {answer: 'That', isCorrect: false}, {answer: 'These', isCorrect: true}]},
                    {id: 5, sample: '... women are teachers.', options: [{answer: 'That', isCorrect: false}, {answer: 'These', isCorrect: true}, {answer: 'This', isCorrect: false}]},
                    {id: 6, sample: '... car is modern.', options: [{answer: 'Those', isCorrect: false}, {answer: 'These', isCorrect: false}, {answer: 'This', isCorrect: true}]},
                    {id: 7, sample: '... children are my friends.', options: [{answer: 'These', isCorrect: true}, {answer: 'That', isCorrect: false}, {answer: 'This', isCorrect: false}]},
                    {id: 8, sample: '... kitten is funny.', options: [{answer: 'Those', isCorrect: false}, {answer: 'This', isCorrect: true}, {answer: 'These', isCorrect: false}]},
                    {id: 9, sample: '... bear is brown.', options: [{answer: 'These', isCorrect: false}, {answer: 'Those', isCorrect: false}, {answer: 'This', isCorrect: true}]},
                    {id: 10, sample: '... animal is wild.', options: [{answer: 'These', isCorrect: false}, {answer: 'This', isCorrect: true}, {answer: 'Those', isCorrect: false}]}
                ]
            },
            {
                test_title: 'тест 2',
                image: null,
                questions: [
                    {id: 1, sample: 'Look at ... snowman over there.', options: [{answer: 'those', isCorrect: false}, {answer: 'that', isCorrect: true}, {answer: 'these', isCorrect: false}]},
                    {id: 2, sample: 'Whose books are ... ?', options: [{answer: 'that', isCorrect: false}, {answer: 'this', isCorrect: false}, {answer: 'these', isCorrect: true}]},
                    {id: 3, sample: '... ships are big.', options: [{answer: 'This', isCorrect: false}, {answer: 'These', isCorrect: true}, {answer: 'That', isCorrect: false}]},
                    {id: 4, sample: 'I want ... cups.', options: [{answer: 'this', isCorrect: false}, {answer: 'that', isCorrect: false}, {answer: 'these', isCorrect: true}]},
                    {id: 5, sample: '... boat over there is not ours.', options: [{answer: 'That', isCorrect: true}, {answer: 'Those', isCorrect: false}, {answer: 'These', isCorrect: false}]},
                    {id: 6, sample: '... flowers are beautiful.', options: [{answer: 'That', isCorrect: false}, {answer: 'These', isCorrect: true}, {answer: 'This', isCorrect: false}]},
                    {id: 7, sample: '... mushrooms are bad.', options: [{answer: 'This', isCorrect: false}, {answer: 'These', isCorrect: true}, {answer: 'That', isCorrect: false}]},
                    {id: 8, sample: '... cheese is yummy!', options: [{answer: 'Those', isCorrect: false}, {answer: 'These', isCorrect: false}, {answer: 'This', isCorrect: true}]},
                    {id: 9, sample: '... snakes are dangerous.', options: [{answer: 'That', isCorrect: false}, {answer: 'These', isCorrect: true}, {answer: 'This', isCorrect: false}]},
                    {id: 10, sample: 'Look at ... house over there.', options: [{answer: 'that', isCorrect: true}, {answer: 'those', isCorrect: false}, {answer: 'these', isCorrect: false}]}
                ]
            },
            {
                test_title: 'тест 3',
                image: null,
                questions: [
                    {id: 1, sample: '... rubber boots are dirty.', options: [{answer: 'That', isCorrect: false}, {answer: 'These', isCorrect: true}, {answer: 'This', isCorrect: false}]},
                    {id: 2, sample: '... spaceship is modern.', options: [{answer: 'This', isCorrect: true}, {answer: 'Those', isCorrect: false}, {answer: 'These', isCorrect: false}]},
                    {id: 3, sample: '... ball is not new.', options: [{answer: 'These', isCorrect: false}, {answer: 'Those', isCorrect: false}, {answer: 'This', isCorrect: true}]},
                    {id: 4, sample: '... teddy bears are mine.', options: [{answer: 'That', isCorrect: false}, {answer: 'These', isCorrect: true}, {answer: 'This', isCorrect: false}]},
                    {id: 5, sample: '... tent is not ours.', options: [{answer: 'Those', isCorrect: false}, {answer: 'That', isCorrect: true}, {answer: 'These', isCorrect: false}]},
                    {id: 6, sample: '... mice are young.', options: [{answer: 'That', isCorrect: false}, {answer: 'These', isCorrect: true}, {answer: 'This', isCorrect: false}]},
                    {id: 7, sample: '... snakes are dangerous.', options: [{answer: 'Those', isCorrect: true}, {answer: 'That', isCorrect: false}, {answer: 'This', isCorrect: false}]},
                    {id: 8, sample: '... apples are sweet.', options: [{answer: 'This', isCorrect: false}, {answer: 'These', isCorrect: true}, {answer: 'That', isCorrect: false}]},
                    {id: 9, sample: '... computer is broken.', options: [{answer: 'Those', isCorrect: false}, {answer: 'That', isCorrect: true}, {answer: 'These', isCorrect: false}]},
                    {id: 10, sample: 'Are ... chairs new?', options: [{answer: 'that', isCorrect: false}, {answer: 'these', isCorrect: true}, {answer: 'this', isCorrect: false}]}
                ]
            },
            {
                test_title: 'тест 4',
                image: null,
                questions: [
                    {id: 1, sample: 'Is ... your car?', options: [{answer: 'those', isCorrect: false}, {answer: 'that', isCorrect: true}, {answer: 'these', isCorrect: false}]},
                    {id: 2, sample: 'Who is ... woman?', options: [{answer: 'these', isCorrect: false}, {answer: 'this', isCorrect: true}, {answer: 'those', isCorrect: false}]},
                    {id: 3, sample: 'Who are ... people?', options: [{answer: 'that', isCorrect: false}, {answer: 'this', isCorrect: false}, {answer: 'these', isCorrect: true}]},
                    {id: 4, sample: 'Where is ... wonderful place?', options: [{answer: 'these', isCorrect: false}, {answer: 'this', isCorrect: true}, {answer: 'those', isCorrect: false}]},
                    {id: 5, sample: '... exercise was easy.', options: [{answer: 'Those', isCorrect: false}, {answer: 'That', isCorrect: true}, {answer: 'These', isCorrect: false}]},
                    {id: 6, sample: "... geese aren't on the farm.", options: [{answer: 'That', isCorrect: false}, {answer: 'This', isCorrect: false}, {answer: 'Those', isCorrect: true}]},
                    {id: 7, sample: '... pictures are on page eleven.', options: [{answer: 'Those', isCorrect: true}, {answer: 'This', isCorrect: false}, {answer: 'That', isCorrect: false}]},
                    {id: 8, sample: '... parrot is clever.', options: [{answer: 'These', isCorrect: false}, {answer: 'Those', isCorrect: false}, {answer: 'This', isCorrect: true}]},
                    {id: 9, sample: '... girl is dancing well.', options: [{answer: 'Those', isCorrect: false}, {answer: 'That', isCorrect: true}, {answer: 'These', isCorrect: false}]},
                    {id: 10, sample: '... house was bult five years ago.', options: [{answer: 'That', isCorrect: true}, {answer: 'Those', isCorrect: false}, {answer: 'These', isCorrect: false}]}
                ]
            },
            {
                test_title: 'тест 5',
                image: null,
                questions: [
                    {id: 1, sample: '... fish are red and orange.', options: [{answer: 'These', isCorrect: true}, {answer: 'That', isCorrect: false}, {answer: 'This', isCorrect: false}]},
                    {id: 2, sample: '... train is from Siberia.', options: [{answer: 'Those', isCorrect: false}, {answer: 'These', isCorrect: false}, {answer: 'That', isCorrect: true}]},
                    {id: 3, sample: 'Look at ... children over there.', options: [{answer: 'those', isCorrect: true}, {answer: 'that', isCorrect: false}, {answer: 'this', isCorrect: false}]},
                    {id: 4, sample: 'I like ... tulips.', options: [{answer: 'that', isCorrect: false}, {answer: 'this', isCorrect: false}, {answer: 'these', isCorrect: true}]},
                    {id: 5, sample: '... frogs are small.', options: [{answer: 'That', isCorrect: false}, {answer: 'These', isCorrect: true}, {answer: 'This', isCorrect: false}]},
                    {id: 6, sample: "I don't like ... balloons.", options: [{answer: 'that', isCorrect: false}, {answer: 'this', isCorrect: false}, {answer: 'those', isCorrect: true}]},
                    {id: 7, sample: 'Take ... books to school tommorrow.', options: [{answer: 'this', isCorrect: false}, {answer: 'these', isCorrect: true}, {answer: 'that', isCorrect: false}]},
                    {id: 8, sample: '... pencil is broken.', options: [{answer: 'Those', isCorrect: false}, {answer: 'That', isCorrect: true}, {answer: 'These', isCorrect: false}]},
                    {id: 9, sample: "Let's buy ... picture.", options: [{answer: 'this', isCorrect: true}, {answer: 'those', isCorrect: false}, {answer: 'these', isCorrect: false}]},
                    {id: 10, sample: '... men are engineers.', options: [{answer: 'This', isCorrect: false}, {answer: 'That', isCorrect: false}, {answer: 'These', isCorrect: true}]}
                ]
            }
        ]
    },
    {
        title: "Глагол 'to be'",
        id: 3,
        info: [
            {
                test_title: 'тест 1',
                image: null,
                questions: [
                    {id: 1, sample: 'Mary ... eleven.', options: [{answer: 'are', isCorrect: false}, {answer: 'is', isCorrect: true}, {answer: 'am', isCorrect: false}]},
                    {id: 2, sample: 'Jim and John ... twins.', options: [{answer: 'is', isCorrect: false}, {answer: 'am', isCorrect: false}, {answer: 'are', isCorrect: true}]},
                    {id: 3, sample: '... you from New York?', options: [{answer: 'Is', isCorrect: false}, {answer: 'Are', isCorrect: true}, {answer: 'Am', isCorrect: false}]},
                    {id: 4, sample: 'I ... from Finland.', options: [{answer: 'am', isCorrect: true}, {answer: 'is', isCorrect: false}, {answer: 'are', isCorrect: false}]},
                    {id: 5, sample: 'Why ... you sad today?', options: [{answer: 'is', isCorrect: false}, {answer: 'am', isCorrect: false}, {answer: 'are', isCorrect: true}]},
                    {id: 6, sample: 'I ... happy to see you again.', options: [{answer: 'are', isCorrect: false}, {answer: 'am', isCorrect: true}, {answer: 'is', isCorrect: false}]},
                    {id: 7, sample: 'It ... not hot today.', options: [{answer: 'is', isCorrect: true}, {answer: 'am', isCorrect: false}, {answer: 'are', isCorrect: false}]},
                    {id: 8, sample: 'The sky ... cloudy.', options: [{answer: 'am', isCorrect: false}, {answer: 'is', isCorrect: true}, {answer: 'are', isCorrect: false}]},
                    {id: 9, sample: 'The clouds ... grey.', options: [{answer: 'are', isCorrect: true}, {answer: 'am', isCorrect: false}, {answer: 'is', isCorrect: false}]},
                    {id: 10, sample: '... your parents on holidays now?', options: [{answer: 'Is', isCorrect: false}, {answer: 'Am', isCorrect: false}, {answer: 'Are', isCorrect: true}]}
                ]
            },
            {
                test_title: 'тест 2',
                image: null,
                questions: [
                    {id: 1, sample: 'Look! The butterfly ... beautiful.', options: [{answer: 'are', isCorrect: false}, {answer: 'is', isCorrect: true}, {answer: 'am', isCorrect: false}]},
                    {id: 2, sample: 'Bees ... yellow and black.', options: [{answer: 'is', isCorrect: false}, {answer: 'are', isCorrect: true}, {answer: 'am', isCorrect: false}]},
                    {id: 3, sample: 'The boys ... in the park.', options: [{answer: 'am', isCorrect: false}, {answer: 'are', isCorrect: true}, {answer: 'is', isCorrect: false}]},
                    {id: 4, sample: 'The children ... at the English lesson now.', options: [{answer: 'are', isCorrect: true}, {answer: 'is', isCorrect: false}, {answer: 'am', isCorrect: false}]},
                    {id: 5, sample: "Don't be afraid. The dog ... friendly.", options: [{answer: 'am', isCorrect: false}, {answer: 'is', isCorrect: true}, {answer: 'are', isCorrect: false}]},
                    {id: 6, sample: 'I ... not in the shop.', options: [{answer: 'are', isCorrect: false}, {answer: 'is', isCorrect: false}, {answer: 'am', isCorrect: true}]},
                    {id: 7, sample: 'Laura ... not my sister.', options: [{answer: 'am', isCorrect: false}, {answer: 'are', isCorrect: false}, {answer: 'is', isCorrect: true}]},
                    {id: 8, sample: 'Sorry, I ... busy.', options: [{answer: 'are', isCorrect: false}, {answer: 'is', isCorrect: false}, {answer: 'am', isCorrect: true}]},
                    {id: 9, sample: 'The apples ... in the basket.', options: [{answer: 'is', isCorrect: false}, {answer: 'are', isCorrect: true}, {answer: 'am', isCorrect: false}]},
                    {id: 10, sample: 'The lemon ... in the fridge.', options: [{answer: 'am', isCorrect: false}, {answer: 'is', isCorrect: true}, {answer: 'are', isCorrect: false}]}
                ]
            },
            {
                test_title: 'тест 3',
                image: null,
                questions: [
                    {id: 1, sample: 'Helen ... ten years old.', options: [{answer: 'are', isCorrect: false}, {answer: 'am', isCorrect: false}, {answer: 'is', isCorrect: true}]},
                    {id: 2, sample: 'She ... a short girl.', options: [{answer: 'are', isCorrect: false}, {answer: 'is', isCorrect: true}, {answer: 'am', isCorrect: false}]},
                    {id: 3, sample: 'Her hair ... short and dark.', options: [{answer: 'is', isCorrect: true}, {answer: 'am', isCorrect: false}, {answer: 'are', isCorrect: false}]},
                    {id: 4, sample: 'Her eyes ... blue.', options: [{answer: 'are', isCorrect: true}, {answer: 'am', isCorrect: false}, {answer: 'is', isCorrect: false}]},
                    {id: 5, sample: 'Her friends ... on the sportsground.', options: [{answer: 'am', isCorrect: false}, {answer: 'is', isCorrect: false}, {answer: 'are', isCorrect: true}]},
                    {id: 6, sample: 'I ... friendy with her.', options: [{answer: 'is', isCorrect: false}, {answer: 'am', isCorrect: true}, {answer: 'are', isCorrect: false}]},
                    {id: 7, sample: "Helen's brothers ... twins.", options: [{answer: 'am', isCorrect: false}, {answer: 'are', isCorrect: true}, {answer: 'is', isCorrect: false}]},
                    {id: 8, sample: 'The boys ... in the kindergarten.', options: [{answer: 'are', isCorrect: true}, {answer: 'is', isCorrect: false}, {answer: 'am', isCorrect: false}]},
                    {id: 9, sample: "Helen's classmate ... her best friend.", options: [{answer: 'am', isCorrect: false}, {answer: 'are', isCorrect: false}, {answer: 'is', isCorrect: true}]},
                    {id: 10, sample: "Helen's parents ... at work now.", options: [{answer: 'am', isCorrect: false}, {answer: 'is', isCorrect: false}, {answer: 'are', isCorrect: true}]}
                ]
            },
            {
                test_title: 'тест 4',
                image: null,
                questions: [
                    {id: 1, sample: 'My bedroom ... big and cosy.', options: [{answer: 'are', isCorrect: false}, {answer: 'am', isCorrect: false}, {answer: 'is', isCorrect: true}]},
                    {id: 2, sample: "Where ... my books? I can't find them.", options: [{answer: 'is', isCorrect: false}, {answer: 'are', isCorrect: true}, {answer: 'am', isCorrect: false}]},
                    {id: 3, sample: 'I ... in the kitchen.', options: [{answer: 'am', isCorrect: true}, {answer: 'is', isCorrect: false}, {answer: 'are', isCorrect: false}]},
                    {id: 4, sample: 'The pupils ... on holidays now.', options: [{answer: 'is', isCorrect: false}, {answer: 'are', isCorrect: true}, {answer: 'am', isCorrect: false}]},
                    {id: 5, sample: 'The flower ... in the vase.', options: [{answer: 'am', isCorrect: false}, {answer: 'are', isCorrect: false}, {answer: 'is', isCorrect: true}]},
                    {id: 6, sample: '... your pet hungry?', options: [{answer: 'Am', isCorrect: false}, {answer: 'Is', isCorrect: true}, {answer: 'Are', isCorrect: false}]},
                    {id: 7, sample: "What ... your favourite film?", options: [{answer: 'am', isCorrect: false}, {answer: 'are', isCorrect: false}, {answer: 'is', isCorrect: true}]},
                    {id: 8, sample: 'Mr. Lens ... from the USA.', options: [{answer: 'are', isCorrect: false}, {answer: 'am', isCorrect: false}, {answer: 'is', isCorrect: true}]},
                    {id: 9, sample: 'I ... not from Moscow.', options: [{answer: 'is', isCorrect: false}, {answer: 'am', isCorrect: true}, {answer: 'are', isCorrect: false}]},
                    {id: 10, sample: '... your classmates at the party?', options: [{answer: 'Am', isCorrect: false}, {answer: 'Are', isCorrect: true}, {answer: 'Is', isCorrect: false}]}
                ]
            },
            {
                test_title: 'тест 5',
                image: null,
                questions: [
                    {id: 1, sample: 'I ... at school at 2 p.m.', options: [{answer: 'was', isCorrect: true}, {answer: 'were', isCorrect: false}]},
                    {id: 2, sample: '... you at work yesterday?', options: [{answer: 'Were', isCorrect: true}, {answer: 'Was', isCorrect: false}]},
                    {id: 3, sample: 'I ... not busy yesterday.', options: [{answer: 'were', isCorrect: false}, {answer: 'was', isCorrect: true}]},
                    {id: 4, sample: '... George at the party?', options: [{answer: 'Was', isCorrect: true}, {answer: 'Were', isCorrect: false}]},
                    {id: 5, sample: 'The children ... sad.', options: [{answer: 'was', isCorrect: false}, {answer: 'were', isCorrect: true}]},
                    {id: 6, sample: 'The cake ... chocolate.', options: [{answer: 'were', isCorrect: false}, {answer: 'was', isCorrect: true}]},
                    {id: 7, sample: 'It ... Monday yesterday.', options: [{answer: 'were', isCorrect: false}, {answer: 'was', isCorrect: true}]},
                    {id: 8, sample: 'The apples ... sweet.', options: [{answer: 'were', isCorrect: true}, {answer: 'was', isCorrect: false}]},
                    {id: 9, sample: '... the book interesting?', options: [{answer: 'Were', isCorrect: false}, {answer: 'Was', isCorrect: true}]},
                    {id: 10, sample: 'Peter and Brian ... not in London last week.', options: [{answer: 'were', isCorrect: true}, {answer: 'was', isCorrect: false}]}
                ]
            },
            {
                test_title: 'тест 6',
                image: null,
                questions: [
                    {id: 1, sample: 'There ... a lot of sweets in the box.', options: [{answer: 'was', isCorrect: false}, {answer: 'were', isCorrect: true}]},
                    {id: 2, sample: 'There ... not much snow last week.', options: [{answer: 'were', isCorrect: false}, {answer: 'was', isCorrect: true}]},
                    {id: 3, sample: 'What present ... in the box?', options: [{answer: 'was', isCorrect: true}, {answer: 'were', isCorrect: false}]},
                    {id: 4, sample: 'There ... not any mistakes in your test.', options: [{answer: 'were', isCorrect: true}, {answer: 'was', isCorrect: false}]},
                    {id: 5, sample: '... there many pictures in the book?', options: [{answer: 'Was', isCorrect: false}, {answer: 'Were', isCorrect: true}]},
                    {id: 6, sample: 'There ... no sugar in the tea.', options: [{answer: 'was', isCorrect: true}, {answer: 'were', isCorrect: false}]},
                    {id: 7, sample: 'There ... a very interesting story in the magazine.', options: [{answer: 'were', isCorrect: false}, {answer: 'was', isCorrect: true}]},
                    {id: 8, sample: 'There ... five boats on the lake.', options: [{answer: 'was', isCorrect: false}, {answer: 'were', isCorrect: true}]},
                    {id: 9, sample: '... there any keys on the table?', options: [{answer: 'Was', isCorrect: false}, {answer: 'Were', isCorrect: true}]},
                    {id: 10, sample: 'The cake ... really tasty.', options: [{answer: 'were', isCorrect: false}, {answer: 'was', isCorrect: true}]}
                ]
            },
            {
                test_title: 'тест 7',
                image: null,
                questions: [
                    {id: 1, sample: 'The book ... in your bag.', options: [{answer: 'is', isCorrect: true}, {answer: 'was', isCorrect: false}, {answer: 'will be', isCorrect: false}]},
                    {id: 2, sample: 'It ... rainy tomorrow.', options: [{answer: 'was', isCorrect: false}, {answer: 'will be', isCorrect: true}, {answer: 'is', isCorrect: false}]},
                    {id: 3, sample: 'The roses ... pink and red.', options: [{answer: 'are', isCorrect: true}, {answer: 'will be', isCorrect: false}, {answer: 'were', isCorrect: false}]},
                    {id: 4, sample: 'The pizza ... tasty at the party yesterday.', options: [{answer: 'was', isCorrect: true}, {answer: 'is', isCorrect: false}, {answer: 'will be', isCorrect: false}]},
                    {id: 5, sample: 'I ... in the mall with my friends next weekend.', options: [{answer: 'was', isCorrect: false}, {answer: 'am', isCorrect: false}, {answer: 'will be', isCorrect: true}]},
                    {id: 6, sample: 'The bus ... late in the morning.', options: [{answer: 'was', isCorrect: true}, {answer: 'is', isCorrect: false}, {answer: 'will be', isCorrect: false}]},
                    {id: 7, sample: 'The cake ... ready in 20 minutes.', options: [{answer: 'was', isCorrect: false}, {answer: 'is', isCorrect: false}, {answer: 'will be', isCorrect: true}]},
                    {id: 8, sample: 'The cheese ... in the fridge.', options: [{answer: 'was', isCorrect: false}, {answer: 'will be', isCorrect: false}, {answer: 'is', isCorrect: true}]},
                    {id: 9, sample: 'It ... snowy and cold yesterday.', options: [{answer: 'will be', isCorrect: false}, {answer: 'was', isCorrect: true}, {answer: 'is', isCorrect:false}]},
                    {id: 10, sample: 'Last Monday the test ... easy.', options: [{answer: 'will be', isCorrect: false}, {answer: 'is', isCorrect: false}, {answer: 'was', isCorrect: true}]}
                ]
            },
        ]
    },
    {
        title: "Present Simple",
        id: 4,
        info: [
            {
                test_title: 'тест 1',
                image: null,
                questions: [
                    {id: 1, sample: 'Kathy ... in the river.', options: [{answer: 'swim', isCorrect: false}, {answer: 'swims', isCorrect: true}]},
                    {id: 2, sample: 'I ... cartoons.', options: [{answer: 'like', isCorrect: true}, {answer: 'likes', isCorrect: false}]},
                    {id: 3, sample: 'John ... a lot of books.', options: [{answer: 'read', isCorrect: false}, {answer: 'reads', isCorrect: true}]},
                    {id: 4, sample: 'Max ... English fluently.', options: [{answer: 'speak', isCorrect: false}, {answer: 'speaks', isCorrect: true}]},
                    {id: 5, sample: 'They ... French well.', options: [{answer: 'speak', isCorrect: true}, {answer: 'speaks', isCorrect: false}]},
                    {id: 6, sample: 'Kevin and Bill ... tennis after school.', options: [{answer: 'play', isCorrect: true}, {answer: 'plays', isCorrect: false}]},
                    {id: 7, sample: 'I ... his parents.', options: [{answer: 'know', isCorrect: true}, {answer: 'knows', isCorrect: false}]},
                    {id: 8, sample: 'The woman ... us.', options: [{answer: 'help', isCorrect: false}, {answer: 'helps', isCorrect: true}]},
                    {id: 9, sample: 'My sisters ... in the park in the evening.', options: [{answer: 'walk', isCorrect: true}, {answer: 'walks', isCorrect: false}]},
                    {id: 10, sample: 'The shop ... at 8 p.m.', options: [{answer: 'close', isCorrect: false}, {answer: 'closes', isCorrect: true}]}
                ]
            },
            {
                test_title: 'тест 2',
                image: null,
                questions: [
                    {id: 1, sample: 'You ... my teacher.', options: [{answer: 'remember', isCorrect: true}, {answer: 'remembers', isCorrect: false}]},
                    {id: 2, sample: 'Cats ... mice.', options: [{answer: 'catch', isCorrect: true}, {answer: 'catches', isCorrect: false}]},
                    {id: 3, sample: 'Lions ... very fast.', options: [{answer: 'run', isCorrect: true}, {answer: 'runs', isCorrect: false}]},
                    {id: 4, sample: 'The girl ... dolls.', options: [{answer: 'collect', isCorrect: false}, {answer: 'collects', isCorrect: true}]},
                    {id: 5, sample: 'The children ... ice-cream in summer.', options: [{answer: 'eat', isCorrect: true}, {answer: 'eats', isCorrect: false}]},
                    {id: 6, sample: 'She ... in the garden on summer days.', options: [{answer: 'work', isCorrect: false}, {answer: 'works', isCorrect: true}]},
                    {id: 7, sample: 'The men ... to fish.', options: [{answer: 'like', isCorrect: true}, {answer: 'likes', isCorrect: false}]},
                    {id: 8, sample: 'Dad and I ... tennis at weekends.', options: [{answer: 'play', isCorrect: true}, {answer: 'plays', isCorrect: false}]},
                    {id: 9, sample: 'Brian ... in London.', options: [{answer: 'live', isCorrect: false}, {answer: 'lives', isCorrect: true}]},
                    {id: 10, sample: 'This flower ... wonderful.', options: [{answer: 'smell', isCorrect: false}, {answer: 'smells', isCorrect: true}]}
                ]
            },
            {
                test_title: 'тест 3',
                image: null,
                questions: [
                    {id: 1, sample: 'This cat ... catch mice.', options: [{answer: "don't", isCorrect: false}, {answer: "doesn't", isCorrect: true}]},
                    {id: 2, sample: 'I ... like autumn.', options: [{answer: "don't", isCorrect: true}, {answer: "doesn't", isCorrect: false}]},
                    {id: 3, sample: 'My mum ... work at the hospital.', options: [{answer: "don't", isCorrect: false}, {answer: "doesn't", isCorrect: true}]},
                    {id: 4, sample: 'Your friend ... help you.', options: [{answer: "doesn't", isCorrect: true}, {answer: "don't", isCorrect: false}]},
                    {id: 5, sample: 'My dad ... drive a car.', options: [{answer: "don't", isCorrect: false}, {answer: "doesn't", isCorrect: true}]},
                    {id: 6, sample: 'These people ... work on the farm.', options: [{answer: "doesn't", isCorrect: false}, {answer: "don't", isCorrect: true}]},
                    {id: 7, sample: 'The frog ... eat snails.', options: [{answer: "don't", isCorrect: false}, {answer: "doesn't", isCorrect: true}]},
                    {id: 8, sample: 'He ... paint well.', options: [{answer: "doesn't", isCorrect: true}, {answer: "don't", isCorrect: false}]},
                    {id: 9, sample: 'These fish ... live in the river.', options: [{answer: "don't", isCorrect: true}, {answer: "doesn't", isCorrect: false}]},
                    {id: 10, sample: 'I ... play hockey in winter.', options: [{answer: "doesn't", isCorrect: false}, {answer: "don't", isCorrect: true}]}
                ]
            }
        ]
    },
    {
        title: "There is/are",
        id: 5,
        info: [
            {
                test_title: 'тест 1',
                image: "living_room",
                questions: [
                    {id: 1, sample: 'There ... not many apples in the basket.', options: [{answer: 'is', isCorrect: false}, {answer: 'are', isCorrect: true}]},
                    {id: 2, sample: 'There ... 22 pupils in my class.', options: [{answer: 'are', isCorrect: true}, {answer: 'is', isCorrect: false}]},
                    {id: 3, sample: '... there any mice in the pantry?', options: [{answer: 'Are', isCorrect: true}, {answer: 'Is', isCorrect: false}]},
                    {id: 4, sample: 'There ... a nice bird in the tree.', options: [{answer: 'is', isCorrect: true}, {answer: 'are', isCorrect: false}]},
                    {id: 5, sample: 'There ... some big houses in the picture.', options: [{answer: 'is', isCorrect: false}, {answer: 'are', isCorrect: true}]},
                    {id: 6, sample: 'There ... not a cloud in the sky.', options: [{answer: 'is', isCorrect: true}, {answer: 'are', isCorrect: false}]},
                    {id: 7, sample: '... there foxes in the forest?', options: [{answer: 'Are', isCorrect: true}, {answer: 'Is', isCorrect: false}]},
                    {id: 8, sample: 'There ... twelve months in the year.', options: [{answer: 'are', isCorrect: true}, {answer: 'is', isCorrect: false}]},
                    {id: 9, sample: '... there any pencils in your bag?', options: [{answer: 'is', isCorrect: false}, {answer: 'are', isCorrect: true}]},
                    {id: 10, sample: 'There ... a pet in my family.', options: [{answer: 'is', isCorrect: true}, {answer: 'are', isCorrect: false}]}
                ]
            },
            {
                test_title: 'тест 2',
                image: null,
                questions: [
                    {id: 1, sample: '... two cars in the garage?', options: [{answer: 'There are', isCorrect: false}, {answer: 'There is', isCorrect: false}, {answer: 'Are there', isCorrect: true}]},
                    {id: 2, sample: '... new pupils in your class?', options: [{answer: 'Are there', isCorrect: true}, {answer: 'There are', isCorrect: false}, {answer: "There aren't", isCorrect: false}]},
                    {id: 3, sample: '... a bird in the park.', options: [{answer: 'Is there', isCorrect: false}, {answer: "There isn't", isCorrect: true}, {answer: "There aren't", isCorrect: false}]},
                    {id: 4, sample: '... ten boys in the team?', options: [{answer: 'Is there', isCorrect: false}, {answer: 'Are there', isCorrect: true}, {answer: "There aren't", isCorrect: false}]},
                    {id: 5, sample: '... a tooth in his mouth.', options: [{answer: "There aren't", isCorrect: false}, {answer: 'Is there', isCorrect: false}, {answer: 'There is', isCorrect: true}]},
                    {id: 6, sample: '... any women in the photo.', options: [{answer: 'Are there', isCorrect: false}, {answer: "There isn't", isCorrect: false}, {answer: "There aren't", isCorrect: true}]},
                    {id: 7, sample: '... three rooms in the flat.', options: [{answer: "There aren't", isCorrect: true}, {answer: "There isn't", isCorrect: false}, {answer: 'Are there', isCorrect: false}]},
                    {id: 8, sample: '... a dog in the yard.', options: [{answer: 'There are', isCorrect: false}, {answer: 'Is there', isCorrect: false}, {answer: 'There is', isCorrect: true}]},
                    {id: 9, sample: '... a window in the hall?', options: [{answer: 'Are there', isCorrect: false}, {answer: "There isn't", isCorrect: false}, {answer: 'Is there', isCorrect: true}]},
                    {id: 10, sample: '... fifteen sweets on the plate.', options: [{answer: 'There are', isCorrect: true}, {answer: 'Are there', isCorrect: false}, {answer: 'There is', isCorrect: false}]}
                ]
            },
            {
                test_title: 'тест 3',
                image: null,
                questions: [
                    {id: 1, sample: '... 26 letters in the alphabet?', options: [{answer: 'Is there', isCorrect: false}, {answer: 'Are there', isCorrect: true}, {answer: 'There are', isCorrect: false}]},
                    {id: 2, sample: '... a TV in your bedroom?', options: [{answer: 'Is there', isCorrect: true}, {answer: 'There is', isCorrect: false}, {answer: "There isn't", isCorrect: false}]},
                    {id: 3, sample: '... a cloud in the sky.', options: [{answer: "There aren't", isCorrect: false}, {answer: 'Is there', isCorrect: false}, {answer: 'There is', isCorrect: true}]},
                    {id: 4, sample: '... any new games on the computer?', options: [{answer: 'Is there', isCorrect: false}, {answer: 'Are there', isCorrect: true}, {answer: "There aren't", isCorrect: false}]},
                    {id: 5, sample: '... any children in the park today.', options: [{answer: "There isn't", isCorrect: false}, {answer: 'Are there', isCorrect: false}, {answer: "There aren't", isCorrect: true}]},
                    {id: 6, sample: '... many boys in your class?', options: [{answer: 'Are there', isCorrect: true}, {answer: "There aren't", isCorrect: false}, {answer: 'There are', isCorrect: false}]},
                    {id: 7, sample: '... some mistakes in the text.', options: [{answer: 'There is', isCorrect: false}, {answer: "Are there", isCorrect: false}, {answer: 'There are', isCorrect: true}]},
                    {id: 8, sample: '... a pen and two pencils on the table.', options: [{answer: 'There are', isCorrect: false}, {answer: 'There is', isCorrect: true}, {answer: 'Is there', isCorrect: false}]},
                    {id: 9, sample: '... six people in your family?', options: [{answer: 'Is there', isCorrect: false}, {answer: "There aren't", isCorrect: false}, {answer: 'Are there', isCorrect: true}]},
                    {id: 10, sample: '... some books on the shelf.', options: [{answer: 'Are there', isCorrect: false}, {answer: 'There are', isCorrect: true}, {answer: "There aren't", isCorrect: false}]}
                ]
            },
            {
                test_title: 'тест 4',
                image: null,
                questions: [
                    {id: 1, sample: 'There ... any coffee left.', options: [{answer: "are", isCorrect: false}, {answer: "aren't", isCorrect: false}, {answer: "isn't", isCorrect: true}]},
                    {id: 2, sample: 'There ... any children in the garden.', options: [{answer: "aren't", isCorrect: true}, {answer: "are", isCorrect: false}, {answer: "isn't", isCorrect: false}]},
                    {id: 3, sample: 'There ... some snow in the yard.', options: [{answer: "are", isCorrect: false}, {answer: "is", isCorrect: true}, {answer: "isn't", isCorrect: false}]},
                    {id: 4, sample: '... there any sugar in my tea?', options: [{answer: "Are", isCorrect: false}, {answer: "Is", isCorrect: true}, {answer: "Aren't", isCorrect: false}]},
                    {id: 5, sample: 'There ... a fireplace in the living room.', options: [{answer: "aren't", isCorrect: false}, {answer: "isn't", isCorrect: true}, {answer: "are", isCorrect: false}]},
                    {id: 6, sample: 'There ... some fruit trees near the house.', options: [{answer: "aren't", isCorrect: false}, {answer: "are", isCorrect: true}, {answer: "is", isCorrect: false}]},
                    {id: 7, sample: 'There ... a fridge in the kitchen.', options: [{answer: "is", isCorrect: true}, {answer: "aren't", isCorrect: false}, {answer: "are", isCorrect: false}]},
                    {id: 8, sample: '... there any ice-cream in the fridge?', options: [{answer: "Are", isCorrect: false}, {answer: "Is", isCorrect: true}, {answer: "Aren't", isCorrect: false}]},
                    {id: 9, sample: 'There ... some bread on the table.', options: [{answer: "are", isCorrect: false}, {answer: "is", isCorrect: true}, {answer: "isn't", isCorrect: false}]},
                    {id: 10, sample: 'There ... any plants in the room.', options: [{answer: "are", isCorrect: false}, {answer: "aren't", isCorrect: true}, {answer: "isn't", isCorrect: false}]}
                ]
            }
        ]
    },
    {
        title: "have/has got",
        id: 6,
        info: [
            {
                test_title: 'тест 1',
                image: null,
                questions: [
                    {id: 1, sample: 'An elephant ... got big ears.', options: [{answer: "have", isCorrect: false}, {answer: "has", isCorrect: true}]},
                    {id: 2, sample: 'Tigers ... got sharp claws.', options: [{answer: "have", isCorrect: true}, {answer: "has", isCorrect: false}]},
                    {id: 3, sample: 'We ... got a big house.', options: [{answer: "have", isCorrect: true}, {answer: "has", isCorrect: false}]},
                    {id: 4, sample: 'My uncle ... got two sons.', options: [{answer: "have", isCorrect: false}, {answer: "has", isCorrect: true}]},
                    {id: 5, sample: 'He ... got a new camera.', options: [{answer: "have", isCorrect: false}, {answer: "has", isCorrect: true}]},
                    {id: 6, sample: 'John ... got a nice colourful kite.', options: [{answer: "have", isCorrect: false}, {answer: "has", isCorrect: true}]},
                    {id: 7, sample: 'I ... got a lot of friends.', options: [{answer: "have", isCorrect: true}, {answer: "has", isCorrect: false}]},
                    {id: 8, sample: 'It ... got a long tail.', options: [{answer: "have", isCorrect: false}, {answer: "has", isCorrect: true}]},
                    {id: 9, sample: 'A giraffe ... got a long neck.', options: [{answer: "have", isCorrect: false}, {answer: "has", isCorrect: true}]},
                    {id: 10, sample: 'Crocodiles ... got big teeth.', options: [{answer: "have", isCorrect: true}, {answer: "has", isCorrect: false}]}
                ]
            },
            {
                test_title: 'тест 2',
                image: null,
                questions: [
                    {id: 1, sample: 'Snakes ... got feet.', options: [{answer: "have", isCorrect: false}, {answer: "has", isCorrect: false}, {answer: "haven't", isCorrect: true}, {answer: "hasn't", isCorrect: false}]},
                    {id: 2, sample: 'Ducks ... got big ears.', options: [{answer: "hasn't", isCorrect: false}, {answer: "have", isCorrect: false}, {answer: "has", isCorrect: false}, {answer: "haven't", isCorrect: true}]},
                    {id: 3, sample: 'Rabbits ... got long ears.', options: [{answer: "has", isCorrect: false}, {answer: "haven't", isCorrect: false}, {answer: "hasn't", isCorrect: false}, {answer: "have", isCorrect: true}]},
                    {id: 4, sample: 'A car ... got four wheels.', options: [{answer: "have", isCorrect: false}, {answer: "has", isCorrect: true}, {answer: "haven't", isCorrect: false}, {answer: "hasn't", isCorrect: false}]},
                    {id: 5, sample: 'A bike ... got four wheels.', options: [{answer: "hasn't", isCorrect: true}, {answer: "have", isCorrect: false}, {answer: "has", isCorrect: false}, {answer: "haven't", isCorrect: false}]},
                    {id: 6, sample: 'Spiders ... got eight legs.', options: [{answer: "haven't", isCorrect: false}, {answer: "hasn't", isCorrect: false}, {answer: "have", isCorrect: true}, {answer: "has", isCorrect: false}]},
                    {id: 7, sample: 'Hamsters ... got short necks.', options: [{answer: "has", isCorrect: false}, {answer: "haven't", isCorrect: false}, {answer: "hasn't", isCorrect: false}, {answer: "have", isCorrect: true}]},
                    {id: 8, sample: 'Birds ... got wings.', options: [{answer: "have", isCorrect: true}, {answer: "has", isCorrect: false}, {answer: "haven't", isCorrect: false}, {answer: "hasn't", isCorrect: false}]},
                    {id: 9, sample: 'Cats ... got long tails.', options: [{answer: "hasn't", isCorrect: false}, {answer: "have", isCorrect: true}, {answer: "has", isCorrect: false}, {answer: "haven't", isCorrect: false}]},
                    {id: 10, sample: 'A frog ... got a tail.', options: [{answer: "haven't", isCorrect: false}, {answer: "hasn't", isCorrect: true}, {answer: "have", isCorrect: false}, {answer: "has", isCorrect: false}]}
                ]
            }
        ]
    },
    {
        title: "Притяжательный падеж имён существительных",
        id: 7,
        info: [
            {
                test_title: 'тест 1',
                image: null,
                questions: [
                    {id: 1, sample: "The ... book is new.", options: [{answer: "pupils'", isCorrect: false}, {answer: "pupil's", isCorrect: true}, {answer: "pupil'", isCorrect: false}]},
                    {id: 2, sample: "The ... bikes are near our school.", options: [{answer: "pupils'", isCorrect: true}, {answer: "pupil's", isCorrect: false}, {answer: "pupil'", isCorrect: false}]},
                    {id: 3, sample: "... rabbit is red.", options: [{answer: "Ben'", isCorrect: false}, {answer: "Ben's", isCorrect: true}, {answer: "Bens'", isCorrect: false}]},
                    {id: 4, sample: "... parrot is white.", options: [{answer: "Dolly's", isCorrect: true}, {answer: "Dollys'", isCorrect: false}, {answer: "Dolly'", isCorrect: false}]},
                    {id: 5, sample: "My ... T-shirt is pink.", options: [{answer: "sister's", isCorrect: true}, {answer: "sister'", isCorrect: false}, {answer: "sisters'", isCorrect: false}]},
                    {id: 6, sample: "The ... box is in the hall.", options: [{answer: "cat's", isCorrect: true}, {answer: "cats'", isCorrect: false}, {answer: "cat'", isCorrect: false}]},
                    {id: 7, sample: "The ... desk is big.", options: [{answer: "teachers'", isCorrect: false}, {answer: "teacher'", isCorrect: false}, {answer: "teacher's", isCorrect: true}]},
                    {id: 8, sample: "Is it your ... bag?", options: [{answer: "mums'", isCorrect: false}, {answer: "mum's", isCorrect: true}, {answer: "mum'", isCorrect: false}]},
                    {id: 9, sample: "His ... car is new.", options: [{answer: "uncle's", isCorrect: true}, {answer: "uncle'", isCorrect: false}, {answer: "uncles'", isCorrect: false}]},
                    {id: 10, sample: "My ... hat is on the shelf.", options: [{answer: "aunts'", isCorrect: false}, {answer: "aunt'", isCorrect: false}, {answer: "aunt's", isCorrect: true}]}
                ]
            },
            {
                test_title: 'тест 2',
                image: null,
                questions: [
                    {id: 1, sample: "I know this ... name.", options: [{answer: "girls'", isCorrect: false}, {answer: "girl's", isCorrect: true}]},
                    {id: 2, sample: "... mother works in the shop.", options: [{answer: "Ben's", isCorrect: true}, {answer: "Bens'", isCorrect: false}]},
                    {id: 3, sample: "Where are the ... school bags?", options: [{answer: "childrens'", isCorrect: false}, {answer: "children's", isCorrect: true}]},
                    {id: 4, sample: "What is your ... address?", options: [{answer: "friends'", isCorrect: false}, {answer: "friend's", isCorrect: true}]},
                    {id: 5, sample: "Her ... name is Steve.", options: [{answer: "husbands'", isCorrect: false}, {answer: "husband's", isCorrect: true}]},
                    {id: 6, sample: "I want to buy ... trainers.", options: [{answer: "mens'", isCorrect: false}, {answer: "men's", isCorrect: true}]},
                    {id: 7, sample: "This ... sons are my classmates.", options: [{answer: "woman's", isCorrect: true}, {answer: "women's", isCorrect: false}]},
                    {id: 8, sample: "My ... hobby is travelling.", options: [{answer: "parent's", isCorrect: false}, {answer: "parents'", isCorrect: true}]},
                    {id: 9, sample: "Can you draw a ... head?", options: [{answer: "tigers'", isCorrect: false}, {answer: "tiger's", isCorrect: true}]},
                    {id: 10, sample: "That ... office is on the first ground.", options: [{answer: "doctor's", isCorrect: true}, {answer: "doctors'", isCorrect: false}]}
                ]
            },
            {
                test_title: 'тест 3',
                image: null,
                questions: [
                    {id: 1, sample: "I have got a friend. My ... computer is old.", options: [{answer: "friends'", isCorrect: false}, {answer: "friend's", isCorrect: true}]},
                    {id: 2, sample: "We often visit our friends. You can at our ... house too.", options: [{answer: "friends'", isCorrect: true}, {answer: "friend's", isCorrect: false}]},
                    {id: 3, sample: "Tom has got a kitten. The ... face is funny.", options: [{answer: "kitten's", isCorrect: true}, {answer: "kittens'", isCorrect: false}]},
                    {id: 4, sample: "There are three boys in the garden. The ... mother is watching them.", options: [{answer: "boys'", isCorrect: false}, {answer: "boy's", isCorrect: true}]},
                    {id: 5, sample: "Bob has got a sister. His ... room is small.", options: [{answer: "sister's", isCorrect: true}, {answer: "sisters'", isCorrect: false}]},
                    {id: 6, sample: "John has got a dog. The ... fur is unusual.", options: [{answer: "dog's", isCorrect: true}, {answer: "dogs'", isCorrect: false}]},
                    {id: 7, sample: "Look at those children. The ... ball is new.", options: [{answer: "childrens'", isCorrect: false}, {answer: "children's", isCorrect: true}]},
                    {id: 8, sample: "This is a strange man. The ... glasses are black.", options: [{answer: "man's", isCorrect: true}, {answer: "men's", isCorrect: false}]},
                    {id: 9, sample: "Ann and Tom has got a puppy. The ... bone is in the kitchen.", options: [{answer: "puppy's", isCorrect: true}, {answer: "puppys'", isCorrect: false}]},
                    {id: 10, sample: "We saw two elephants in the zoo. The ... ears are big.", options: [{answer: "elephants'", isCorrect: true}, {answer: "elephant's", isCorrect: false}]}
                ]
            },
        ]
    },
    {
        title: "Притяжательные местоимения",
        id: 8,
        info: [
            {
                test_title: 'тест 1',
                image: null,
                questions: [
                    {id: 1, sample: "Jane has got a bike. ... bike is new.", options: [{answer: "My", isCorrect: false}, {answer: "Her", isCorrect: true}, {answer: "Your", isCorrect: false}]},
                    {id: 2, sample: "We go to school. ... school is big and old.", options: [{answer: "Your", isCorrect: false}, {answer: "Their", isCorrect: false}, {answer: "Our", isCorrect: true}]},
                    {id: 3, sample: "They have got a car. ... car is in the garage.", options: [{answer: "Their", isCorrect: true}, {answer: "My", isCorrect: false}, {answer: "Your", isCorrect: false}]},
                    {id: 4, sample: "She has got long wavy hair. ... hair is nice.", options: [{answer: "His", isCorrect: false}, {answer: "Your", isCorrect: false}, {answer: "Her", isCorrect: true}]},
                    {id: 5, sample: "My cousin has got a scooter. ... scooter is broken.", options: [{answer: "His", isCorrect: true}, {answer: "Her", isCorrect: false}, {answer: "Their", isCorrect: false}]},
                    {id: 6, sample: "The pupils have got a lot of text-books. ... text-books are old.", options: [{answer: "Their", isCorrect: true}, {answer: "Your", isCorrect: false}, {answer: "Our", isCorrect: false}]},
                    {id: 7, sample: "Bob has got a dog. ... dog is angry.", options: [{answer: "Its", isCorrect: false}, {answer: "Her", isCorrect: false}, {answer: "His", isCorrect: true}]},
                    {id: 8, sample: "Mary has got a dress. ... dress is long.", options: [{answer: "His", isCorrect: false}, {answer: "Your", isCorrect: false}, {answer: "Her", isCorrect: true}]},
                    {id: 9, sample: "I have got a new friend. ... friend is from London.", options: [{answer: "My", isCorrect: true}, {answer: "Your", isCorrect: false}, {answer: "Our", isCorrect: false}]},
                    {id: 10, sample: "My mother has got a handbag. ... handbag is in the hall now.", options: [{answer: "My", isCorrect: false}, {answer: "Her", isCorrect: true}, {answer: "Our", isCorrect: false}]}
                ]
            },
            {
                test_title: 'тест 2',
                image: null,
                questions: [
                    {id: 1, sample: "Kathie likes playing the guitar and ... sister likes singing songs.", options: [{answer: "my", isCorrect: false}, {answer: "our", isCorrect: false}, {answer: "her", isCorrect: true}]},
                    {id: 2, sample: "Where is John? ... mother is waiting for him.", options: [{answer: "His", isCorrect: true}, {answer: "Her", isCorrect: false}, {answer: "Our", isCorrect: false}]},
                    {id: 3, sample: "Do you like this book? ... title is 'Swan Song'.", options: [{answer: "His", isCorrect: false}, {answer: "Her", isCorrect: false}, {answer: "Its", isCorrect: true}]},
                    {id: 4, sample: "Mike and I are in the part today. ... roller skates are broken.", options: [{answer: "His", isCorrect: false}, {answer: "My", isCorrect: false}, {answer: "Our", isCorrect: true}]},
                    {id: 5, sample: "Look at the cat! ... tail is fluffy.", options: [{answer: "Her", isCorrect: false}, {answer: "Its", isCorrect: true}, {answer: "His", isCorrect: false}]},
                    {id: 6, sample: "My mum doesn't go to hospital on Friday. It is ... day-off.", options: [{answer: "Her", isCorrect: true}, {answer: "My", isCorrect: false}, {answer: "Our", isCorrect: false}]},
                    {id: 7, sample: "Tom is a driver. ... lorry is in the garage.", options: [{answer: "His", isCorrect: true}, {answer: "Our", isCorrect: false}, {answer: "Your", isCorrect: false}]},
                    {id: 8, sample: "Jim and George are friends. ... parents are friends too.", options: [{answer: "Our", isCorrect: false}, {answer: "Your", isCorrect: false}, {answer: "Their", isCorrect: true}]},
                    {id: 9, sample: "Do you see that elephant? ... trunk is long.", options: [{answer: "Their", isCorrect: false}, {answer: "His", isCorrect: false}, {answer: "Its", isCorrect: true}]},
                    {id: 10, sample: "Where are you? ... parents are looking for you.", options: [{answer: "Your", isCorrect: true}, {answer: "Our", isCorrect: false}, {answer: "Their", isCorrect: false}]}
                ]
            },
            {
                test_title: 'тест 3',
                image: null,
                questions: [
                    {id: 1, sample: "... friends live in the USA.", options: [{answer: "My", isCorrect: true}, {answer: "Mine", isCorrect: false}]},
                    {id: 2, sample: "My name is Jack. What is ...?", options: [{answer: "your", isCorrect: false}, {answer: "yours", isCorrect: true}]},
                    {id: 3, sample: "Her name is Jane. -Sorry, what is ... name?", options: [{answer: "her", isCorrect: true}, {answer: "hers", isCorrect: false}]},
                    {id: 4, sample: "Is it ... pen?", options: [{answer: "your", isCorrect: true}, {answer: "yours", isCorrect: false}]},
                    {id: 5, sample: "This is your phone. Where is ...?", options: [{answer: "my", isCorrect: false}, {answer: "mine", isCorrect: true}]},
                    {id: 6, sample: "The black car is ... .", options: [{answer: "their", isCorrect: false}, {answer: "theirs", isCorrect: true}]},
                    {id: 7, sample: "You can take ... mobile.", options: [{answer: "my", isCorrect: true}, {answer: "mine", isCorrect: false}]},
                    {id: 8, sample: "... dog is really clever.", options: [{answer: "Your", isCorrect: true}, {answer: "Yours", isCorrect: false}]},
                    {id: 9, sample: "This house is not ... .", options: [{answer: "our", isCorrect: false}, {answer: "ours", isCorrect: true}]},
                    {id: 10, sample: "They aren't my trainers. They are ... .", options: [{answer: "her", isCorrect: false}, {answer: "hers", isCorrect: true}]}
                ]
            }
        ]
    },
    {
        title: "Объектные местоимения",
        id: 9,
        info: [
            {
                test_title: 'тест 1',
                image: null,
                questions: [
                    {id: 1, sample: 'Test', options: [{answer: "wrong", isCorrect: false}, {answer: "correct", isCorrect: true}]}
                ]
            }
        ]
    },
    {
        title: "Модальные глаголы",
        id: 10,
        info: [
            {
                test_title: 'тест 1',
                image: null,
                questions: [
                    {id: 1, sample: 'Test', options: [{answer: "wrong", isCorrect: false}, {answer: "correct", isCorrect: true}]}
                ]
            }
        ]
    },
    {
        title: "Past Simple",
        id: 11,
        info: [
            {
                test_title: 'тест 1',
                image: null,
                questions: [
                    {id: 1, sample: 'Test', options: [{answer: "wrong", isCorrect: false}, {answer: "correct", isCorrect: true}]}
                ]
            }
        ]
    },
    {
        title: "Future Simple",
        id: 12,
        info: [
            {
                test_title: 'тест 1',
                image: null,
                questions: [
                    {id: 1, sample: 'Test', options: [{answer: "wrong", isCorrect: false}, {answer: "correct", isCorrect: true}]}
                ]
            }
        ]
    },
    {
        title: "Present Continues",
        id: 13,
        info: [
            {
                test_title: 'тест 1',
                image: null,
                questions: [
                    {id: 1, sample: 'Test', options: [{answer: "wrong", isCorrect: false}, {answer: "correct", isCorrect: true}]}
                ]
            }
        ]
    },
    {
        title: "Степени сравнения прилагательных",
        id: 14,
        info: [
            {
                test_title: 'тест 1',
                image: null,
                questions: [
                    {id: 1, sample: 'Test', options: [{answer: "wrong", isCorrect: false}, {answer: "correct", isCorrect: true}]}
                ]
            }
        ]
    },
]

export default dataChoice;