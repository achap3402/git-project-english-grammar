import React from "react";
import { useParams } from "react-router-dom";
import { useState,useEffect } from "react";

import data from "../../Data/DataChoice";

import { DetailItem } from "../../Components/DetailItem";

import './styles.scss';

const DetailPage = () => {
    const {id} = useParams();
    const {test_title} = useParams();
    const [tests, setTests] = useState(null);

    useEffect(() => {
        setTests(data[id].info);
    }, [id])

    return (
        tests ?
        <div className="detail">
            <div className="title-category-wrapper">
                <h3 className="title-category">{data[id].title}</h3>
            </div>
            {tests.filter(item => item.test_title === test_title).map((item, index) => (
                <DetailItem key={index} test={item.questions} title={item.test_title} image={item.image}/>
            ))}
        </div> : 
        <></>
    );
};

export {DetailPage};