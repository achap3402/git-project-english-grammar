import React, {useState, useEffect} from "react";
import data from "../../Data/DataChoice";
import { useParams } from "react-router-dom";
import { Link } from "react-router-dom";

import './styles.scss';

const ListPage = () => {
    const {id} = useParams();
    const [tests, setTests] = useState(null);

    useEffect(() => {
        setTests(data[id].info);
    }, [id]);

    return (
        tests ?
        <div className="list">
            <nav>
                <h1 className="title-list">{data[id].title}</h1>
                {
                    tests.map((item, index) => (
                        <Link key={index} to={`/detail/${id}/${item.test_title}`}>
                            <div className="number_test">тест {index + 1}</div>
                        </Link>
                    ))
                }
            </nav>
        </div> :
        <></>
    );
};

export {ListPage};