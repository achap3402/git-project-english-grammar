import React from "react";
import { ContactsItem } from "../../Components/ContactsItem";

import './styles.scss';

const Contacts = () => {
    return (
        <>
        <section className="page_contacts">
          <ContactsItem />
        </section>
        </>
    );
};

export {Contacts};