import React from "react";
import { NavLink } from "react-router-dom";

import Logo from '../../assets/logo/logo_umbrella.png';

import './styles.scss';

const MainPage = () => {
    return (
        <div className="main-wrapper">
            <div className="main-body">
                <img src={Logo} alt="logo"  className="logo"/>
                <NavLink className="main-link btn-round" to='/category'>S T A R T</NavLink>
                <div className="main-body-right">
                    <h1 className="main-body-english-grammar">English <br /> Grammar</h1>
                    <h3 className="main-body-for-kids">f o r&nbsp;&nbsp;&nbsp; k i d s</h3>
                </div>
            </div>
        </div>
    );
};

export {MainPage};