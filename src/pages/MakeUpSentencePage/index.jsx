import React, { useState } from "react";
import exam from "../../Data/Exam";
import './styles.scss';

const MakeUpSentencePage = () => {

    const [cardList, setCardList] = useState(exam);

    const [currentCard, setCurrentCard] = useState(null);

    const dragStartHandler = (e, card) => {
        setCurrentCard(card);
    }

    const dragEndHandler = (e, card) => {
        e.target.style.background ='none';
    }

    const dragOverHandler = (e, card) => {
        e.preventDefault();
        e.target.style.background = 'black';
    }
    
    const dropHandler = (e, card) => {
        e.preventDefault();
        setCardList(cardList.map(c => {
            if (c.id === card.id) {
                return {...c, order: currentCard.order}
            };
            if (c.id === currentCard.id) {
                return {...c, order: card.order}
            };
            return c;
        }))
        e.target.style.background = 'none';
    }

    const sortCards = (a, b) => {
        if (a.order > b.order) {
            return 1
        } else {
            return -1
        }
    }

    const handleClick = () => {

    }

    return (
        <div className="dragAndDropPage">
            {cardList.sort(sortCards).map((card, index) =>
            <div
                key={index}
                onDragStart={(e) => dragStartHandler(e, card)}
                onDragLeave={(e) => dragEndHandler(e)}
                onDragEnd={(e) => dragEndHandler(e)}
                onDragOver={(e) => dragOverHandler(e)}
                onDrop={(e) => dropHandler(e, card)}
                draggable={true}
                className="card">
                {card.text}
            </div>
            )}
            <button onClick={() => handleClick()}>Unswer</button>
        </div>
    )
};

export {MakeUpSentencePage};