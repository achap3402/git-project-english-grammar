import React from "react";
import data from "../../Data/DataChoice";
import { Link } from "react-router-dom";

import './styles.scss';

const CategoryPage = () => {
    return (
        <div className="category">
            <nav className="nav">
                {
                    data.map(test => (
                        <Link key={test.id} to={`/${test.id}`}>
                            <div className="title">{test.title}</div>
                        </Link>
                    ))
                }
            </nav>
        </div>
    );
};

export {CategoryPage};