import { Outlet } from "react-router-dom";
import { Header } from "../Header";
import { Footer } from "../Footer";

import './styles.scss';

const Layout = () => {
    return (
        <>
        <div className="app-content-wrapper">
            <div className="content-wrapper">
                <Header />
                <main className="main-content">
                    <Outlet />
                </main>
                <Footer />
            </div>
        </div>
        </>
    );
};

export {Layout};