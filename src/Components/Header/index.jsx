import { useState } from "react";
import { NavLink } from 'react-router-dom';
import Logo from '../../assets/logo/logo_umbrella.png';
import './styles.scss';

const Header = () => {
  const [active, setActive] = useState(false);

  const toggle = () => {
    setActive(!active);
  }

    return (
      <header className='header'>
        <div className='container'>
          <span></span>
          <div className='header-logo'>
            <img src={Logo} alt="logo"  className="logo"/>
          </div>
          <div className={!active ? 'header-burger' : 'header-burger header-burger--active'} onClick={() => toggle()}>
            <span></span>
          </div>
          <nav className={!active ? 'header-menu' : 'header-menu header-menu--active'}>
            <NavLink to='/' onClick={() => toggle()}>Main</NavLink>
            <NavLink to='/category' onClick={() => toggle()}>Category</NavLink>
            <NavLink to='/contacts' onClick={() => toggle()}>Contacts</NavLink>
          </nav>
        </div>
      </header>
    );
  }
  
  export {Header};