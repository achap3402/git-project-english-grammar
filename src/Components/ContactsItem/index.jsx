import { NavLink } from "react-router-dom";

import './styles.scss';

const ContactsItem = () => {
    return (
        <div className="contacts_item">
            <div className="contacts">
                <div>

                <p>Mobail:</p>
                </div>
                <NavLink to="tel:+79606981200">+79606981200</NavLink>
            </div>
            <div className="contacts">
                <div>

                <p>Telegram:</p>
                </div>
                <NavLink to="https://t.me/EugeneChaplygina">@EugeneChaplygina</NavLink>
            </div>
            <div className="contacts">
                <div>

                <p>WhatsApp:</p>
                </div>
                <NavLink to="https://wa.me/9606981200">wa.me/9606981200</NavLink>
            </div>
            <div className="contacts">
                <div>

                <p>Email:</p>
                </div>
                <NavLink to="mailto:chaplygin.2021@mail.ru">chaplygin.2021@mail.ru</NavLink>
            </div>
            <div className="contacts">
                <div>

                <p>VK:</p>
                </div>
                <NavLink to="https://vk.com/id72842578">vk.com/id72842578</NavLink>
            </div>
        </div>
    )
}

export {ContactsItem};