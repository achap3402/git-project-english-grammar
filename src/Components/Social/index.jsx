import './styles.scss';
import { NavLink } from 'react-router-dom';

const Social = () => {
    return (
        <div className="social">
            <NavLink className='social-icon phone' title='call me' to='tel:+79606981200'>
                <i className="fa-solid fa-phone"></i>
            </NavLink>
            <NavLink className='social-icon telegram' title='telegram' to='https://t.me/EugeneChaplygina'>
                <i className="fa-regular fa-paper-plane"></i>
            </NavLink>
            <NavLink className='social-icon whatsapp'title='whatsapp' to='https://wa.me/9606981200'>
                <i className="fa-brands fa-whatsapp"></i>
            </NavLink>
            <NavLink className='social-icon mail' title='mail' to='mailto:chaplygin.2021@mail.ru'>
                <i className="fa-regular fa-at"></i>
            </NavLink>
            <NavLink className='social-icon vk' title='VK' to='https://vk.com/id72842578'>
                <i className="fa-brands fa-vk"></i>
            </NavLink>
        </div>
    )
}

export {Social};