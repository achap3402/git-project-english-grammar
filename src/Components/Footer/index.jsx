import { Social } from '../Social';

import './styles.scss';

const Footer = () => {
    return (
      <footer className="footer">
        <div className="footer-content">
          <div className='footer-social'>
            <Social/>
          </div>
        </div>
      </footer>
    );
  }
  
  export {Footer};