import React from "react";
import "./styles.scss";

const Modal = ({active, setActive, children}) => {
    return (
        <div className={active ? "modal active" : "modal"} onClick={() => setActive(false)}>
            <div className={active ? "modal-content active" : "modal-content"} onClick={e => e.stopPropagation()}>
                {children}
                <button className="btn-close" onClick={() => setActive(false)}>X</button>
            </div>
        </div>
    );
}

export {Modal};