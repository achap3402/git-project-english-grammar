import { useState } from "react";
import { Modal } from "../Modal";
import Image from '../../assets/img/living_room.png';

import './styles.scss';

const DetailItem = props => {
    const [activeSample, setActiveSample] = useState(0);
    const [modalActive, setModalActive] = useState(false);
    const [score, setScore] = useState(0);
    const [showResult, setShowResult] = useState(false);

    const handleClick = (isCorrect) => {
        
        if (isCorrect) {
            setScore(score + 1);
        }

        const nexSample = activeSample + 1;

        if (nexSample < props.test.length) {
            setActiveSample(activeSample + 1);
        } else {
            setShowResult(true);
        }
    };

    const mixArr = (arr) => {
        return arr.map(i => [Math.random(), i]).sort().map(i => i[1])
    };

    return (
        showResult ? 
            <div className="result">
                <Modal active={modalActive} setActive={setModalActive}>
                    <p>Ваш результат: <span>{score}</span> из <span>{props.test.length}</span></p>
                </Modal>
                <button className="result-btn btn-round" onClick={() => setModalActive(true)}>
                    R E S U L T
                </button>
            </div> :
            <section className="detail-item-wrapper">
                <div className="detail-item-top">
                    <div className="item-top">
                        <h5 className="item-top-title">{props.title}</h5>
                        <p className="item-top-mission">Выберите правильный вариант ответа.</p>
                        <p className="item-top-track">вопрос <span>{activeSample + 1}</span>/<span>{props.test.length}</span></p>
                        {props.image !== null ? <img className="image" src={Image} alt="living_room"/> : <></>}
                    </div>
                </div>
                <div className="detail-item-bottom">
                    <div className="item-bottom">
                        <div className="item-bottom-sample">{props.test[activeSample].sample}</div>
                        <div className="item-bottom-option">
                            {mixArr(props.test[activeSample].options).map((item, index) => (
                                <div className="bottom-option" onClick={() => handleClick(item.isCorrect)} key={index} >
                                    {item.answer}
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </section>
    )
}

export {DetailItem};