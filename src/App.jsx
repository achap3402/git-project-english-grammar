import React from 'react';
import { Routes, Route } from 'react-router-dom';

import { MainPage } from './pages/MainPage';
import { ListPage } from './pages/ListPage';
import { DetailPage } from './pages/DetailPage';
import { MakeUpSentencePage } from './pages/MakeUpSentencePage';
import { NotFoundPage } from './pages/NotFoundPage';
import { CategoryPage } from './pages/CategoryPage';
import { Contacts } from './pages/Contacts';


import { Layout } from './Components/Layout';

import './App.scss';

const App = () => {
  return (
    <>
      <Routes>
        <Route path='/' element={<Layout />} >
          <Route index element={<MainPage />} />
          <Route path='/:id' element={<ListPage />} />
          <Route path='category' element={<CategoryPage />} />
          <Route path='detail/:id/:test_title' element={<DetailPage />} />
          <Route path='makeupsentence' element={<MakeUpSentencePage />} />
          <Route path='contacts' element={<Contacts />} />
          <Route path='*' element={<NotFoundPage />} />
        </Route>
      </Routes>
    </>
  );
};

export default App;